<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("shaun");
echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold-Blooded : " . $sheep->cold_blooded . "<br>"; // "no"

$frog = new frog("buduk");
echo "<br> Name : " . $frog->name . "<br>"; 
echo "Legs : " . $frog->legs . "<br>"; 
echo "Cold-Blooded : " . $frog->cold_blooded . "<br>";
echo "Jump : " . $frog->jump . "<br>"; 

$ape = new ape("kera sakti");
echo "<br> Name : " . $ape->name . "<br>"; 
echo "Legs : " . $ape->legs . "<br>"; 
echo "Cold-Blooded : " . $ape->cold_blooded . "<br>"; 
echo "Yell : " . $ape->yell . "<br>"; 

?>